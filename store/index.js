export const state = () => ({
    users: [],
    posts: [],
    onePost:{},
    photos: []
})

export const getters = {
    getUsers: state => state.users,
    getPosts: state => state.posts,
    getOnePost: state => state.onePost,
    getPhotos: state => state.photos,
}

export const mutations = {
    setUsersData(state, data) {
        state.users = data
    },
    setPostsData(state, data) {
        state.posts = data
    },
    setOnePost(state, data){
        state.onePost = data
    },
    setPhotos(state, data) {
        state.photos = data
    },
}

export const actions = {
    async getAllUsers(context) {
        await this.$axios
            .get(`admin/contractors/get?status=MODERATION'`)
            .then((response) => {
                context.commit('setUsersData', response.data.content)
            })
            .catch((e) => {
                console.log(e);
            });
    },
    async getAllPosts(context) {
        await this.$axios
            .get(`admin/contracts/get?dir=ASC`)
            .then((response) => {
                context.commit('setPostsData', response.data.content)
            })
            .catch((e) => {
                console.log(e);
            });
    },
    async workWithUsers(context, data) {
        await this.$axios
            .patch(`admin/contractors/change?status=${data.parameter}`, data.id)
            .then((response) => {
                this.dispatch('getAllUsers')
            })
            .catch((e) => {
                console.log(e);
            });
    },
    async workWithOrders(context, data) {
        await this.$axios
            .patch(`admin/contracts/change?status=${data.parameter}`, data.id)
            .then((response) => {
                this.dispatch('getAllPosts')
            })
            .catch((e) => {
                console.log(e);
            });
    },
    async getOnePost(context, data) {
        await this.$axios
            .get(`admin/contracts/get?id=${data}`)
            .then((response) => {
                context.commit('setOnePost', response.data.content[0])
            })
            .catch((e) => {
                console.log(e);
            });
    },
    async getPhotos(context, data) {
        await this.$axios
            .get(`files/media/get?value=${data}`
                , {
                    responseType: "blob"
                }
            )
            .then((response) => {
                context.commit('setPhotos', response.data)
            })
            .catch((e) => {
                console.log(e);
            });
    }
}